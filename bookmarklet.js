var parts = window.location.href.match('https?://bitbucket.org/?([a-z]*)/?([a-z]*)');
var owner = parts[1];
var repo = parts[2];
var usr = prompt('Bitbucket email:');
var pass = prompt('Bitbucket password:');
var cred = btoa(usr+':'+pass);
$('#issues-toolbar').append('<button id="close-all-btn" class="aui-button aui-button-primary">Close All Issues</button>');
$('#close-all-btn').on('click', () => {
  var issues = $('#issues-list').find('a.execute');
  for(var i = 0; i < issues.length; i++) {
    var id = issues[i].title.match('#?([0-9]*):')[1];
    $.ajax({
      url: 'https://api.bitbucket.org/1.0/repositories/'+owner+'/'+repo+'/issues/'+id,
      beforeSend: (xhr) => {
        xhr.setRequestHeader("Authorization", "Basic " + cred);
      },
      method: 'PUT',
      data: {
        status: 'closed'
      },
      success: () => {
        console.log('success!');
      },
      error: (xhr) => {
        console.log(xhr.responseText);
      }
    });
  }
});