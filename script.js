const user = "bdubose@doozer.com"
const pw = "BitBucket457"
const repo = "issuetest"
const owner = "bdubose"

const resultsList = $('#res_list');
const dialog = document.getElementById('dialog');
const filterInput = $('#filters_input');
const resultsUris = [];

$('#query_btn').on('click', () => {
  var query = encodeURI(filterInput.val());
  console.log(query);
  resultsList.html('');
  $.ajax({
    url: `https://api.bitbucket.org/1.0/repositories/${owner}/${repo}/issues?${query}`,
    beforeSend: (xhr) => {
      xhr.setRequestHeader("Authorization", "Basic " + btoa(`${user}:${pw}`));
    },
    success: (data) => {
      data.issues.forEach((issue) => {
        resultsUris.push(issue.resource_uri);
        resultsList.append(`<li>${issue.status}-${issue.title}</li>`)
      });
    },
    error: (xhr) => {
      console.log(xhr);
    }
  });
});

$('#go_btn').on('click', () => {
  resultsUris.forEach((uri) => {
    $.ajax({
      url: `https://api.bitbucket.org${uri}`,
      beforeSend: (xhr) => {
        xhr.setRequestHeader("Authorization", "Basic " + btoa(`${user}:${pw}`));
      },
      method: 'PUT',
      data: {
        status: 'open'
      },
      success: () => {
        console.log('success!');
      },
      error: (xhr) => {
        console.log(xhr);
      }
    });
    console.log(uri);
  });
});